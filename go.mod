module bitbucket.org/nurtureapp/go-mhttp

go 1.12

require (
	github.com/elazarl/goproxy v0.0.0-20190711103511-473e67f1d7d2 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/parnurzeal/gorequest v0.2.15
	github.com/pkg/errors v0.8.1
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
)
