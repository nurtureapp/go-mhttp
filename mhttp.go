package mhttp

import (
	"bytes"
	"io/ioutil"
	"net/http"

	"github.com/parnurzeal/gorequest"
	"github.com/pkg/errors"
)

// Handle .
func Handle(resp gorequest.Response, body []byte, errs []error) []error {
	if len(errs) > 0 {
		return errs
	}

	if resp.StatusCode != 200 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return []error{errors.Wrapf(err, "Error reading body in non 200 request")}
		}

		return []error{errors.New(string(body))}
	}

	return nil
}

// Request .
func Request(method string, url string, apiKey string, input interface{}) *gorequest.SuperAgent {
	req := gorequest.New()
	methodMap := map[string]func(string) *gorequest.SuperAgent{
		http.MethodGet:    req.Get,
		http.MethodPost:   req.Post,
		http.MethodPut:    req.Put,
		http.MethodDelete: req.Delete,
	}
	reqsa, ok := methodMap[method]
	if !ok {
		panic("invalid request method")
	}

	return reqsa(url).
		Set("Content-Type", "application/json").
		Set("Authorization", "Bearer "+apiKey).
		Set("Accept", "application/json").
		Send(input)
}

// Verify .
func Verify(resp gorequest.Response, errs []error) error {
	if len(errs) > 0 {
		return JoinErrors(errs)
	}

	if resp.StatusCode != 200 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Wrapf(err, "Error reading body in non 200 request")
		}

		return errors.New(string(body))
	}

	return nil
}

// JoinErrors .
func JoinErrors(errs []error) error {
	var b bytes.Buffer
	for _, e := range errs {
		b.WriteString(e.Error())
	}
	return errors.New(b.String())
}
